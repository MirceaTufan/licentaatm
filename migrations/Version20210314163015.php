<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314163015 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE calendar (id INT AUTO_INCREMENT NOT NULL, class_group_id INT NOT NULL, course_id INT NOT NULL, date DATETIME NOT NULL, INDEX IDX_6EA9A1464A9A1217 (class_group_id), INDEX IDX_6EA9A146591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE calendar ADD CONSTRAINT FK_6EA9A1464A9A1217 FOREIGN KEY (class_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE calendar ADD CONSTRAINT FK_6EA9A146591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE course ADD duration VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE `group` ADD teacher_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_6DC044C541807E1D FOREIGN KEY (teacher_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_6DC044C541807E1D ON `group` (teacher_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE calendar');
        $this->addSql('ALTER TABLE course DROP duration');
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_6DC044C541807E1D');
        $this->addSql('DROP INDEX IDX_6DC044C541807E1D ON `group`');
        $this->addSql('ALTER TABLE `group` DROP teacher_id');
    }
}
