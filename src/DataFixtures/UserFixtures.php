<?php


namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Parser;

class UserFixtures  extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $users = (new Parser())->parse(file_get_contents(__DIR__."/Data/UsersData.yml"));
        foreach($users as $user) {
            $newUser = (new User())
                ->setEmail($user["email"])
                ->setRoles([$user["role"]])
                ->setIsVerified(true);
            ;
            $newUser->setPassword($this->encoder->encodePassword($newUser, $user["password"]));
            $manager->persist($newUser);
        }
        $manager->flush();
    }
}