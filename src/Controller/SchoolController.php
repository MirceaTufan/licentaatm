<?php


namespace App\Controller;

use App\Entity\School;
use App\Entity\User;
use App\Form\SchoolType;
use App\Repository\SchoolRepository;
use App\Repository\TestRepository;
use App\Util\Pagination;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/school", name="school.")
 */
class SchoolController extends AbstractController
{

    public function __construct()
    {
    }

    /**
     * @Route("/add/{school}", name="add", defaults={"school"=null})
     * @param Request $request
     * @return Response
     */
    public function add(?School $school, Request $request)
    {
        if(!$school instanceof School){
            $school = new School();
        }

        $form = $this->createForm(SchoolType::class, $school);
        $form->add("submit", SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $school = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($school);
            $entityManager->flush();
            $this->addFlash("success", "School was saved!");
            return $this->redirectToRoute('school.list');
        }
        return $this->render("school/add.html.twig", ["form" => $form->createView()]);
    }


    /**
     * @Route("-list/{page}", name="list", defaults={"page"=1})
     * @param int $page
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function list(int $page, SchoolRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        /** @var User $user */
        $user = $this->getUser();
        $query = $request->query->get('q');
        if($this->isGranted("ROLE_ADMIN")) {
            $user = null;
        }
        $queryBuilder = $repository->getWithSearchQueryBuilder($query);
        $pagination = $paginator->paginate(
            $queryBuilder,
            $page,
            Pagination::ITEMS_PER_PAGE
        );
        return $this->render('school/list.html.twig', [
            'pagination' => $pagination
        ]);
    }
}