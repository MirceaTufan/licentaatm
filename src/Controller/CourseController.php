<?php


namespace App\Controller;

use App\Entity\Course;
use App\Entity\Module;
use App\Entity\User;
use App\Form\CourseType;
use App\Repository\CourseRepository;
use App\Util\Pagination;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/course", name="course.")
 */
class CourseController extends AbstractController
{

    public function __construct()
    {
    }

    /**
     * @Route("/add/{course}", name="add", defaults={"course"=null})
     * @param Request $request
     * @return Response
     */
    public function add(?Course $course, Request $request)
    {
        if(!$course instanceof Course){
            $course = new Course();
        }

        $form = $this->createForm(CourseType::class, $course);
        $form->add("submit", SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $course = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $file = $form['video']->getData();
            $fileName = md5(rand(0,20)).".". $file->guessExtension();
            $file->move(__DIR__."/../../public/video", $fileName );
            $course->setVideo($fileName);
            $entityManager->persist($course);
            $entityManager->flush();
            $this->addFlash("success", "Course was saved!");
            return $this->redirectToRoute('module.view', ["module" => $course->getModule()->getId()]);
        }
        return $this->render("course/add.html.twig", ["form" => $form->createView()]);
    }


    /**
     * @Route("/view/{course}", name="view")
     */
    public function view(Course $course)
    {

        return $this->render('course/view.html.twig', [
            'course' => $course
        ]);
    }
}