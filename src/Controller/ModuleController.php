<?php


namespace App\Controller;

use App\Entity\Module;
use App\Entity\User;
use App\Form\ModuleType;
use App\Repository\ModuleRepository;
use App\Repository\TestRepository;
use App\Util\Pagination;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/module", name="module.")
 */
class ModuleController extends AbstractController
{

    public function __construct()
    {
    }

    /**
     * @Route("/add/{module}", name="add", defaults={"module"=null})
     * @param Request $request
     * @return Response
     */
    public function add(?Module $module, Request $request)
    {
        if(!$module instanceof Module){
            $module = new Module();
        }

        $form = $this->createForm(ModuleType::class, $module);
        $form->add("submit", SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $module = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($module);
            $entityManager->flush();
            $this->addFlash("success", "Module was saved!");
            return $this->redirectToRoute('module.list');
        }
        return $this->render("module/add.html.twig", ["form" => $form->createView()]);
    }


    /**
     * @Route("-list/{page}", name="list", defaults={"page"=1})
     * @param int $page
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function list(int $page, ModuleRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        /** @var User $user */
        $user = $this->getUser();
        $query = $request->query->get('q');
        if($this->isGranted("ROLE_ADMIN")) {
            $user = null;
        }
        $queryBuilder = $repository->getWithSearchQueryBuilder($query);
        $pagination = $paginator->paginate(
            $queryBuilder,
            $page,
            Pagination::ITEMS_PER_PAGE
        );
        return $this->render('module/list.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/view/{module}", name="view")
     */
    public function view(Module $module)
    {

        return $this->render('module/view.html.twig', [
            'module' => $module
        ]);
    }
}