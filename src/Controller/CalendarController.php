<?php


namespace App\Controller;

use App\Entity\Calendar;
use App\Entity\Module;
use App\Entity\User;
use App\Form\CalendarType;
use App\Repository\CalendarRepository;
use App\Service\CalendarService;
use App\Util\Pagination;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/calendar", name="calendar.")
 */
class CalendarController extends AbstractController
{

    public function __construct()
    {
    }

    /**
     * @Route("/add/{calendar}", name="add", defaults={"calendar"=null})
     * @param Request $request
     * @return Response
     */
    public function add(?Calendar $calendar, Request $request)
    {
        if(!$calendar instanceof Calendar){
            $calendar = new Calendar();
        }

        $form = $this->createForm(CalendarType::class, $calendar);
        $form->add("submit", SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $calendar = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($calendar);
            $entityManager->flush();
            $this->addFlash("success", "Calendar was saved!");
            return $this->redirectToRoute('module.view', ["module" => $calendar->getModule()->getId()]);
        }
        return $this->render("calendar/add.html.twig", ["form" => $form->createView()]);
    }


    /**
     * @Route("/view", name="view")
     */
    public function view()
    {

        return $this->render('calendar/view.html.twig');
    }

    /**
     * @Route("/all", name="all")
     */
    public function all(CalendarService $service)
    {
        return $this->json([
            "success"=> 1,
            "result"=> $service->getAllCalendars()
        ]);
    }
}