<?php


namespace App\Controller;

use App\Entity\Group;
use App\Entity\User;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use App\Util\Pagination;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/group", name="group.")
 */
class GroupController extends AbstractController
{

    public function __construct()
    {
    }

    /**
     * @Route("/add/{group}", name="add", defaults={"group"=null})
     * @param Request $request
     * @return Response
     */
    public function add(?Group $group, Request $request)
    {
        if(!$group instanceof Group){
            $group = new Group();
        }

        $form = $this->createForm(GroupType::class, $group);
        $form->add("submit", SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $group = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($group);
            $entityManager->flush();
            $this->addFlash("success", "Group was saved!");
            return $this->redirectToRoute('group.list');
        }
        return $this->render("group/add.html.twig", ["form" => $form->createView()]);
    }


    /**
     * @Route("-list/{page}", name="list", defaults={"page"=1})
     * @param int $page
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function list(int $page, GroupRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        /** @var User $user */
        $user = $this->getUser();
        $query = $request->query->get('q');
        if($this->isGranted("ROLE_ADMIN")) {
            $user = null;
        }
        $queryBuilder = $repository->getWithSearchQueryBuilder($query);
        $pagination = $paginator->paginate(
            $queryBuilder,
            $page,
            Pagination::ITEMS_PER_PAGE
        );
        return $this->render('group/list.html.twig', [
            'pagination' => $pagination
        ]);
    }
}