<?php


namespace App\Form;


use App\Entity\Group;
use App\Entity\Module;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

class CalendarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label' => 'name',
                'choice_value' => function (?Group $entity) {
                    return $entity ? $entity->getId() : '';
                },
            ])
            ->add('module', EntityType::class, [
                'class' => Module::class,
                'choice_label' => 'name',
                'choice_value' => function (?Module $entity) {
                    return $entity ? $entity->getId() : '';
                },
            ])
        ;
    }

}