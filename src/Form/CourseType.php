<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\Group;
use App\Entity\Module;
use App\Entity\School;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('module', EntityType::class, [
                'class' => Module::class,
                'choice_label' => 'name',
                'choice_value' => function (?Module $entity) {
                    return $entity ? $entity->getId() : '';
                },
            ])
            ->add('description')
            ->add('sort')
            ->add('duration')
            ->add('video', FileType::class, [

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
