<?php


namespace App\Service;


use App\Entity\Calendar;
use App\Repository\CalendarRepository;
use Symfony\Component\Routing\RouterInterface;

class CalendarService
{
    /**
     * @var CalendarRepository
     */
    private $calendarRepository;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(CalendarRepository $calendarRepository, RouterInterface $router)
    {

        $this->calendarRepository = $calendarRepository;
        $this->router = $router;
    }
    public function getAllCalendars()
    {
        $result = [];
        $calendars = $this->calendarRepository->findAll();
        /** @var Calendar $calendar */
        foreach($calendars as $calendar) {
            $startDate = new \DateTime($calendar->getDate()->format("Y-m-d H:i:s"));
            $result[] = [
                "id" => $calendar->getId(),
                "url" => $this->router->generate('course.view', ['course' => $calendar->getCourse()->getId()]),
                "title" => $calendar->getCourse()->getModule()->getName()." | ".$calendar->getCourse()->getName()." | ".$calendar->getClassGroup()->getName(),
                "class" => "event-success",
                "start" =>  strtotime($startDate->format("Y-m-d H:i:s")) . '000',
                "end" =>  strtotime($startDate->modify("+".$calendar->getCourse()->getDuration()." minutes")->format("Y-m-d H:i:s")) . '000',
            ];
        }
        return $result;
    }
}