<?php


namespace App\Twig;


use App\Entity\Bookkeeper;
use App\Entity\Company;
use App\Entity\Property;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigFunctions extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions()
    {
        return [
        ];
    }


    public function image($path)
    {
        return '/assets/'.$path;
    }
    public function css($path)
    {
        return '/css/'.$path;
    }
    public function js($path)
    {
        return '/js/'.$path;
    }
    public function plugins($path)
    {
        return '/plugins/'.$path;
    }
}
